-- ######################################################
--           Ingotor finishing lua tube 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this lua tube check if the process is finished to reset the machine

-- ** events **
if event.type == "item" then
    interrupt(30,"is_finished") 
    return "black"
elseif event.type == "interrupt" then  
    if event.iid == "is_finished" then
        digiline_send("processing",{step = "is_finished"})
    end
end
