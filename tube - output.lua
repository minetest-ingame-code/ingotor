-- ######################################################
--               Ingotor output lua tube 
--         by Guill4um on august, 21th of 2020
-- ######################################################

-- this lua tube sent process result items to the good chest 
-- depending on player owner detected when the machine start

-- ** events ** 
if event.type == "item" then
    if mem.is_locked_chest ~= nil and  mem.is_locked_chest then
        return "red" -- sent to locked chest for private use
    end
    return "white" -- sent to unclocked chest for public use
elseif event.type == "digiline" then -- on a digiline message is recieved
    if event.channel == "settings" then 
         -- reset and free memory
        if event.msg == "CLEAR" then
            mem.is_locked_chest = nil
            -- settings
        elseif event.msg.action == "SET_OUTPUT" then
            if event.msg.is_locked_chest then
                mem.is_locked_chest = true
            else
                mem.is_locked_chest = false
            end  
        end
    end
end